package ea1;

import ea1.crud.Create;
import ea1.crud.Find;
import ea1.crud.Update;

//https://classroom.google.com/u/0/c/MTQ1MDA3NzAyMTcw/a/Mjc5NjkyNjYyNzI5/details

public class Main {

    public static void main(String[] args) {

        // Crea dos estudiants més
        //Create.insert();
        // Mostra les dades dels students del teu grup.
        Find.findByGroup("A");
        // Mostra les dades del students que tenen un 100 al exam
        Find.findByScoreExam(100d);
        // Mostra les dades dels estudiants que tenen menys d’un 50 al exam
        Find.findByScoreMenor(50d);
        // Mostra els interessos del student amb student_id=123
        Find.selectStudentById(123);
        // Actualitza un document en concret
        Update.updateById("name", "Pere", "Pere MODIFICAT");

    }

}
