package ea1.crud;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Create {


    public static void insert() {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");

        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");

        MongoCollection<Document> gradesCollection = sampleTrainingDB.getCollection("grades");
        Document student = new Document("_id", new ObjectId());

        student.append("student_id", 124)
                .append("name", "Kenneth")
                .append("surname", "Griñan Ferre")
                .append("class_id", "DAM")
                .append("group", "A")
                .append("scores", asList(
                        new Document("type", "exam").append("score", 100d),
                        new Document("type", "teamWork").append("score", 50d))
                );
        //gradesCollection.insertOne(student);


        Document student1 = new Document("_id", new ObjectId());
        student1.append("student_id", 125)
                .append("name", "Invented name")
                .append("class_id", "Undefined")
                .append("group", "A")
                .append("interests", asList("music","gym","code","electronics"));


        List<Document> students = new ArrayList<>();
        students.add(student);
        students.add(student1);

        gradesCollection.insertMany(students);
        System.out.println("Insert 2 documents");

        mongoClient.close();
    }
}
