package ea1.crud;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.*;

public class Find {

    public static void exampleFind() {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("grades").find();
        System.out.println("************** Tots els students **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));
        System.out.println("************** Els que no tenen nom **************");
        listGrades = sampleTrainingDB.getCollection("grades").find(new Document("name", null));
        listGrades.forEach( document -> System.out.println(document.toJson()));
        System.out.println("***** Els que tenen nota de homework amb 0 *****");
        listGrades = sampleTrainingDB.getCollection("grades").find( eq ("scores", Document.parse ("{'type': 'homework', 'score': 0.0 }")));
        listGrades.forEach( document -> System.out.println(document.toJson()));
        mongoClient.close();
    }

    public static void findByGroup(String group) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("grades").find( eq ("group", group));

        System.out.println("Students of group " + group);
        listGrades.forEach((student) -> {
            System.out.println(student);
        });
        mongoClient.close();
    }

    //Mostra les dades del students que tenen un 100 al exam
    public static void findByScoreExam(Double score) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("grades").find( eq ("scores", Document.parse ("{'type': 'exam', 'score': "+score+" }")));

        System.out.println("Students of score exam is " + score);
        listGrades.forEach((student) -> {
            System.out.println(student);
        });
        mongoClient.close();
    }

    //Mostra les dades dels estudiants que tenen menys d’un 50 al exam
    public static void findByScoreMenor(Double score) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("grades").find( lt ("scores", Document.parse ("{'type': 'exam', 'score': "+score+" }")));

        listGrades = sampleTrainingDB.getCollection("grades").find(
                and(
                eq("scores.type", "exam"),
                lt("scores.type", 50.0)));


        System.out.println("Students of score exam menor a " + score);
        listGrades.forEach((student) -> {
            System.out.println(student);
        });
        mongoClient.close();
    }

    //Mostra els interessos del student amb student_id=123.
    public static void selectStudentById(int id) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        //Document student = (Document) sampleTrainingDB.getCollection("grades").find({"student_id", id}).limit(1);

        FindIterable<Document> listGrades;

        listGrades = sampleTrainingDB.getCollection("grades").find(new Document("student_id", id)).limit(1);

        System.out.println("Students of id = " + id);
        listGrades.forEach(System.out::println);
        mongoClient.close();
    }
}
