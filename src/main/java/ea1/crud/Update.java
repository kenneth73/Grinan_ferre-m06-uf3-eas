package ea1.crud;

import com.mongodb.BasicDBObject;
import com.mongodb.client.*;

public class Update {
// https://kb.objectrocket.com/mongo-db/how-to-update-a-document-in-mongodb-using-java-384
    public static void updateById(String camp, String valorBuscat, String nouValor) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://kenneth:kenneth@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("sample_training");
        //Document student = (Document) sampleTrainingDB.getCollection("grades").find({"student_id", id}).limit(1);


        BasicDBObject query = new BasicDBObject();
        query.put(camp, valorBuscat); // (1)


        BasicDBObject newDocument = new BasicDBObject();
        newDocument.put(camp, nouValor); // (2)

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$set", newDocument); // (3)

        sampleTrainingDB.getCollection("grades").updateOne(query, updateObject);

        mongoClient.close();

    }
}
