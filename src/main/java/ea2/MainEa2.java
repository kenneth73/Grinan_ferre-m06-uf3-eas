package ea2;
import com.google.gson.Gson;
import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import ea2.pojo.Person;
import ea2.pojo.Product;
import org.bson.Document;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class MainEa2 {

    static String user = "kenneth";
    static String pass = "kenneth";
    static String database = "organization";

    public static void main(String[] args) {
        //EXERCICI 1
        //carregaDocuments(database, "people", "resources/people.json");

        //EXERCICI 2
        //carregaCollection(database, "people", "resources/people.json");

        //EXERCICI 3
        //dropCollection ("organization", "people");

        //EXERCICI 4
        //dropDatabase("organization");

        //EXERCICI 5
        //insertProducts("company", "products", "resources/products.json");
        //esborraProductByPrice("company", "products", 400, 600);

        //EXERCICI 6
        //actualitzaStockProductsByPrice ("company", "products", 400, 600, 0);

    }

    static final void carregaDocuments(String databaseName, String collection, String pathFile) {

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(pathFile))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);
        
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collectionPeople = myDB.getCollection(collection);

        ArrayList<Document> arrayList = new ArrayList<>();

        for(int i =0; i<people.length; i++) {
            String personJSON = gson.toJson(people[i]);
            System.out.println(personJSON);
            arrayList.add(Document.parse(personJSON));

            //collectionPeople.insertOne(Document.parse(personJSON));
        }

        collectionPeople.insertMany(arrayList);

        mongoClient.close();

    }

    static final void carregaCollection(String databaseName, String collection, String file) {

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collectionPeople = myDB.getCollection(collection);

        for(int i =0; i<people.length; i++) {
            String personJSON = gson.toJson(people[i]);
            System.out.println(personJSON);
            collectionPeople.insertOne(Document.parse(personJSON));
        }
        mongoClient.close();
    }

    static final void dropCollection (String databaseName, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);

        MongoCollection collectionMongo = myDB.getCollection(collection);
        myDB.getCollection(collection).drop();

        if (collectionMongo.countDocuments() == 0) {
            System.out.println("La collection \"" + collection + "\" s'ha esborrat correctament");
        } else {
            System.out.println("La collection \"" + collection + "\" NO s'ha esborrat correctament");
            System.out.println("Queden " + collectionMongo.countDocuments() + " documents");
        }
        mongoClient.close();
    }

    static final void dropDatabase (String databaseName) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        //ERROR myDB.drop();
        myDB.getCollection(databaseName).drop();
        mongoClient.close();
    }

    static final void esborraProductByPrice(String databaseName, String collection, int minPrice, int maxPrice) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collectionPeople = myDB.getCollection(collection);

        /*FindIterable<Document> listProducts;
        listProducts = myDB.getCollection(collection)
                .find(and(
                        gt("price", minPrice),
                        lt("price", maxPrice)));
        listProducts.forEach(System.out::println);*/

        DeleteResult deleteResultCompany = collectionPeople.deleteMany(and(
                gt("price", minPrice),
                lt("price", maxPrice)));

        System.out.println("S'han esborrat " + deleteResultCompany.getDeletedCount() + " documents");

        mongoClient.close();
    }

    static final void actualitzaStockProductsByPrice (String databaseName, String collection, int minPrice, int maxPrice, int newPrice) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collectionProducts = myDB.getCollection(collection);

        System.out.println("update products que price >= " + minPrice + " and " +"price <= " + maxPrice + " , new price=  " + newPrice);

        UpdateResult updateResult = collectionProducts.updateMany(and(
                gt("price", minPrice),
                lt("price", maxPrice)),set("price", newPrice));


        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");

    }

    static final void insertProducts(String databaseName, String collection, String pathFile) {
        List<Product> productList = new ArrayList<>();

        ArrayList<Document> documents = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(pathFile))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                documents.add(Document.parse(linea));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.avm1e.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collectionProduct = myDB.getCollection(collection);
        collectionProduct.insertMany(documents);

        mongoClient.close();

    }
}
