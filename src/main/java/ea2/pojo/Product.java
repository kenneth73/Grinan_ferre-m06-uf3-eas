package ea2.pojo;

import java.util.Arrays;

public class Product {

    //{ "name": "MacBook", "price": 1299, "stock": 10, "picture": "macbook.jpeg", "categories": ["macbook", "notebook"]}

    String name;
    Double price;
    int stock;
    String picture;
    String[] categories;

    public Product(String name, Double price, int stock, String picture, String[] categories) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.picture = picture;
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Product{" + "name='" + name + '\'' + ", price=" + price + ", stock=" + stock + ", picture='" + picture + '\'' + ", categories=" + Arrays.toString(categories) + '}';
    }
}
