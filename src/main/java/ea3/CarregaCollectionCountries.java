package ea3;

import com.google.gson.Gson;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import ea3.pojo.CountryDTO;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
 * Llegeix un fitxer amb array Ex: [{},{},{}]
 * I fa els insert
 */
public class CarregaCollectionCountries {
    static String user = "";
    static String pass = "";
    static String database = "";
    static String host = "";

    public static void main(String[] args) {

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("resources/countries.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        CountryDTO[] countries = gson.fromJson(stringFile, CountryDTO[].class);

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@"+host+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionPeople = myDB.getCollection("countries");

        for(int i =0; i<countries.length; i++) {
            String countryJSON = gson.toJson(countries[i]);
            collectionPeople.insertOne(Document.parse(countryJSON));
        }

        mongoClient.close();
    }
}
