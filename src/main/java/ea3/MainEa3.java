package ea3;

import com.google.gson.Gson;
import com.mongodb.MongoWriteException;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import ea3.pojo.CountryDTO;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

//Per millorar la llegibilitat dels nostres fragments, podem afegir una importació estàtica:
import static com.mongodb.client.model.Aggregates.*;

import static org.junit.Assert.*;

public class MainEa3 {

    static String USER = "kenneth";
    static String PASS = "kenneth";
    static String DATABASENAME = "ea3";
    static String HOST = "cluster0.avm1e.mongodb.net";

    public static void main(String[] args) {
        // 1
        //carregaCountries("ea3", "countries", "resources/countries.json");
        // 2
        //crearIndex("ea3", "countries", "name", true);
        //2
        //listaIndex("ea3", "countries");
        //2
        //insertaCountry("ea3", "countries", new Document("name","spain"));
        //printCollection("ea3", "countries");

        //givenCountryCollection_whenEnglishSpeakingCountriesCounted_thenNinetyOne(DATABASENAME, "countries");

        //givenCountryCollection_whenCountedRegionWise_thenMaxInAfrica(DATABASENAME, "countries");

        //givenCountryCollection_whenAreaSortedDescending_thenSuccess(DATABASENAME, "countries");

        //givenCountryCollection_whenNeighborsCalculated_thenMaxIsFifteenInChina(DATABASENAME, "countries");
    }

    public static void carregaCountries(String database, String collection, String file){
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("resources/countries.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        CountryDTO[] countries = gson.fromJson(stringFile, CountryDTO[].class);

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(DATABASENAME);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);

        ArrayList<Document> countriesJsonList = new ArrayList<>();

        for (CountryDTO c: countries){
            String countryJson = gson.toJson(c);
            countriesJsonList.add(Document.parse(countryJson));
        }

        collectionCountries.insertMany(countriesJsonList);
        mongoClient.close();

    }

    //exercicis 2
    public static void crearIndex(String database, String collection, String key, boolean unique) {

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(DATABASENAME);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);


        //CREAMOS NUEVO INDICE DE CLAVE UNICA, PARA EVITAR DOCUMENTOS DUPLICADOS
        //*** SI ESE CAMPO-VALOR ESTA DUPLICADO NO TE DEJARA CREAR EL INDICE ***
        IndexOptions indexOptions = new IndexOptions().unique(true);
        //INDICE DEL CAMPO NAME
        collectionCountries.createIndex(Indexes.ascending("name"), indexOptions);


        //Podem generar-ne d’altres.
        //Aquests index els podem definir també de clau única, i així evitar que s’introdueixin elements duplicats.
        collectionCountries.createIndex(Indexes.ascending("picture"));

        mongoClient.close();
        //Font: http://mongodb.github.io/mongo-java-driver/3.4/driver/tutorials/indexes/
    }

    public static void listaIndex(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);
        ListIndexesIterable<Document> index = collectionCountries.listIndexes();
        System.out.println("Index de la coleccio " + collection + " (database= "+ database + ")");
        index.forEach(document -> System.out.println(document.toJson()));
        mongoClient.close();
    }

    public static void insertaCountry(String database, String collection, Document country) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);
        try {
            collectionCountries.insertOne(country);
        } catch (MongoWriteException mongoWriteException) {
            System.out.println("Error al insertar Document");
            mongoWriteException.printStackTrace();
        }

        mongoClient.close();
    }
    //end exercicis 2

    //exercicis 3
    /*Exercici 3. Us d’agregacions
    Llegeix aquest post, (el punt 3 el pots descartar) https://www.baeldung.com/java-mongodb-aggregations
    Fes les proves que es troben en el punt 4. Aquests exemples parteixen de la col·lecció que hem insertat en aquesta
    EA3.
    Revisa els exemples i implementa’ls en funcions i cridales des del main. Posa-li noms que t’ajudin a entendre que fa
    cada metode.
    Notes: no utilitzis la anotació @Test a sobre de les funcions
    Documentació Mongo sobre aggregation:
    https://docs.mongodb.com/manual/reference/sql-aggregation-comparison/*/


    //coincideix i compta
    public static void givenCountryCollection_whenEnglishSpeakingCountriesCounted_thenNinetyOne(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);
        Document englishSpeakingCountries = collectionCountries.aggregate(Arrays.asList(
                match(Filters.eq("languages.name", "English")),
                count())).first();

        System.out.println("Numero de países de habla inglesa:");
        System.out.println(englishSpeakingCountries.toJson());

        assertEquals(91, englishSpeakingCountries.get("count"));
    }


    //agrupa (amb suma ) i ordena
    public static void givenCountryCollection_whenCountedRegionWise_thenMaxInAfrica(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);
        Document maxCountriedRegion = collectionCountries.aggregate(Arrays.asList(
                group("$region", Accumulators.sum("tally", 1)),
                sort(Sorts.descending("tally")))).first();

        System.out.println(maxCountriedRegion.toJson());

        assertTrue(maxCountriedRegion.containsValue("Africa"));
    }

    //ordenar, limitar i sortir
    public static void givenCountryCollection_whenAreaSortedDescending_thenSuccess(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);

        System.out.println("7 countries que no esten el la collection largest_sevent");
        collectionCountries.aggregate(Arrays.asList(
                sort(Sorts.descending("area")),
                limit(7),
                out("largest_seven"))).forEach((e)-> System.out.println(e.toJson()));

        MongoCollection<Document> largestSeven = myDB.getCollection("largest_seven");

        assertEquals(7, largestSeven.countDocuments());

        Document usa = largestSeven.find(Filters.eq("alpha3Code", "USA")).first();

        System.out.println("Document alpha3Code, value USA");

        assertNotNull(usa);
    }

    //projecte, grup (amb màxim), coincidència
    public static void givenCountryCollection_whenNeighborsCalculated_thenMaxIsFifteenInChina(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);

        Bson borderingCountriesCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        int maxValue = collectionCountries.aggregate(Arrays.asList(borderingCountriesCollection,
                group(null, Accumulators.max("max", "$borderingCountries"))))
                .first().getInteger("max");

        assertEquals(15, maxValue);

        Document maxNeighboredCountry = collectionCountries.aggregate(Arrays.asList(borderingCountriesCollection,
                match(Filters.eq("borderingCountries", maxValue)))).first();

        assertTrue(maxNeighboredCountry.containsValue("China"));
    }

    //UTILS
    public static void printCollection(String database, String collection) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionCountries = myDB.getCollection(collection);
        //recorrer todos los registros
        collectionCountries.find().forEach(c-> System.out.println(c));
        mongoClient.close();
    }

}
