package ea3.pojo;

public class MyCountry {

    private String name;
    private String[] topLevelDomain;
    private String alpha2Code;
    private String alpha3Code;
    private String[] callingCodes;
    private String capital;
    private String[] altSpellings;
    private String region;
    private String subregion;
    private int population;
    private LatLng[] latLang;

    private String demonym;
    private float area;
    private float gini;
    private String[] timezones;
    private String[] borders;
    private String nativeName;
    private String numericCode;

    private Concurrencies[] currencies;
    private Languages[] languages;
    private Translations[] translations;
    private String flag;
    private RegionalBlocs[] regionalBlocs;
    private String cioc;


    private class LatLng {
        float lat;
        float lan;
    }

    private class Concurrencies {
        String code;
        String name;
        String symbol;
    }

    private class Languages {
        String iso639_1;
        String iso639_2;
        String name;
        String nativeName;
    }

    private class Translations{
        String de;
        String es;
        String fr;
        String ja;
        String it;
        String br;
        String pt;
        String nl;
        String hr;
        String fa;
    }

    private class RegionalBlocs {
        String acronym;
        String name;
        String[] otherAcronyms;
        String[] otherNames;
    }



}
