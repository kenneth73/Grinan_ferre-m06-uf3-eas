package ea3.pojo;

import java.util.List;
import java.io.Serializable;

public class RegionalBlocsDTO implements Serializable {
	private String acronym;
	private String name;
	private List<Object> otherAcronyms;
	private List<Object> otherNames;

	public void setAcronym(String acronym){
		this.acronym = acronym;
	}

	public String getAcronym(){
		return acronym;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setOtherAcronyms(List<Object> otherAcronyms){
		this.otherAcronyms = otherAcronyms;
	}

	public List<Object> getOtherAcronyms(){
		return otherAcronyms;
	}

	public void setOtherNames(List<Object> otherNames){
		this.otherNames = otherNames;
	}

	public List<Object> getOtherNames(){
		return otherNames;
	}

	@Override
 	public String toString(){
		return 
			"RegionalBlocsDTO{" + 
			"acronym = '" + acronym + '\'' + 
			",name = '" + name + '\'' + 
			",otherAcronyms = '" + otherAcronyms + '\'' + 
			",otherNames = '" + otherNames + '\'' + 
			"}";
		}
}